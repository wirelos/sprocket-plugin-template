#include "TemplatePlugin.h"

TemplatePlugin::TemplatePlugin(TemplateConfig cfg)
{
    templateConfig = cfg;
}

void TemplatePlugin::activate(Scheduler *scheduler)
{
    PRINT_MSG(Serial, "TEMPLATE", "plugin activated");
}
