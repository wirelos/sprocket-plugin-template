#ifndef __TEMPLATE_PLUGIN__
#define __TEMPLATE_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include <Plugin.h>
#include "utils/utils_print.h"

using namespace std;
using namespace std::placeholders;

struct TemplateConfig
{
    const char *var;
};

class TemplatePlugin : public Plugin
{
  public:
    TemplateConfig templateConfig;
    TemplatePlugin(TemplateConfig cfg);
    void activate(Scheduler *scheduler);
};

#endif