#ifndef __DEVICE_CONFIG__
#define __DEVICE_CONFIG__

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

// Chip config
#define SPROCKET_TYPE       "SPROCKET"
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       1000

// network config
#define SPROCKET_MODE       1
#define WIFI_CHANNEL        11
#define AP_SSID             "sprocket"
#define AP_PASSWORD         "th3r31sn0sp00n"
#define STATION_SSID        "MyAP"
#define STATION_PASSWORD    "th3r31sn0sp00n"
#define HOSTNAME            "sprocket"
#define CONNECT_TIMEOUT     10000

#endif